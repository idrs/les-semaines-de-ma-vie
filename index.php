<?php 
// composer autoloader for required packages and dependencies
require_once('lib/autoload.php');



/** @var \Base $f3 */
$f3 = \Base::instance();
// F3 autoloader for application business code
$f3->set('AUTOLOAD', 'app/');

class Fooo {
	function get(\Base $f3, $params) {
		echo "Hello World.";

	}
}

$db = new DB\Jig('db/data/',DB\Jig::FORMAT_JSON);
$f3->set('user',new DB\Jig\Mapper($db,'users'));
//$user->load(array('@userID=?','tarzan'));

$f3->route('GET /','Fooo->get');

$f3->route('GET /@ident',
    function($f3) {
      $user = $f3->get('user');
      $result = $user->load(array(
        '@userid = ?',
        $f3->get('PARAMS.ident')
        )
      );
      if($result){
        $user->visits++;
        $user->save();
      echo "<p>Visit number ".$user->visits ."</p>";
      $t = floor(1000*microtime(true));
      echo $t;
      $f3->set('userid',base_convert(implode('',array_reverse(str_split($t))),10,36));
      echo "<p>". $f3->get('userid') ."</p>";}
      else {
        echo "Empty array";
        $user->userid = $f3->get('PARAMS.ident');
        $user->visits = 1;
        $user->save();
      }
      echo \Template::instance()->render('app/views/page.html');	
            /**if($user->visits){ 
        $user->visits++;
        $user->save();
        echo $f3->get('PARAMS.ident').' is your ident ! You visit '.$user->visits.' times.';
      }
      else {
        $user->visits=1;
        $user->save();
        echo $f3->get('PARAMS.ident').' is your ident ! You visit for the first time.';
      
      }*/
    }
);

$f3->route('POST /@ident',
  function($f3,$params){
    echo "<p>Tu as POSTé pour ".$f3->get('PARAMS.ident')."</p>";
    echo "<p>Tu as POSTé ".$params."</p>";
  }

);

$f3->run();
