var EmptyLife = function(){return range(0,90).map((y)=>range(0,52).map((w)=>{return -1})); }

var Life = {
  list: [],
  legende:[['école',"#00FF00"],['encore',"#ff0000"]],
  pactive:null,
  id: userid,
  init: function(){
    this.list = EmptyLife();
    //this.id = Date.now().toString(36);
  },
  
}



var LifeBoard = {
    nevName:"test",
    nevColor: "#000000",
    color: "#0000ff",
    circleStart: [],
    circleEnd: [],
    cercledeb: null,
    cerclefin: null,
    mode: 0,
    updateLife: function(){
      
      console.log("update",LifeBoard.cercledeb,LifeBoard.cerclefin);
      var y = LifeBoard.cercledeb[0];
      var w = LifeBoard.cercledeb[1];
      while(y<LifeBoard.cerclefin[0] || w<=LifeBoard.cerclefin[1]){
          Life.list[y][w]=Life.pactive;
          w++
          if(w>52){
            y++;
            w=0;
          }
      
      }
/*      Life.list = Life.list.map((y,ynum)=>y.map( 
              (w,wnum)=> {if (wnum>=LifeBoard.cercledeb[1] && 
                              wnum<=LifeBoard.cerclefin[1] &&
                              ynum>=LifeBoard.cercledeb[0] && 
                              ynum<=LifeBoard.cerclefin[0]){
                                Life.pactive
                              }
                          } ))*/
      LifeBoard.cercledeb =null;
      console.log(Life.list);
    },
    oninit: Life.init(),
    oncreate: function(){
      var qrcode = new QRCode(document.getElementById("qrcode"), {
	        text: "http://prof.vinet.free.fr/les-semaines-de-ma-vie/"+Life.id,
            width: 64,
            height: 64,
            colorDark : "#000000",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.H
          });
    }, 
    view: function() {
        //console.log(Life.legende,Life.pactive);
        var lifelist = Life.list;
        if(LifeBoard.cercledeb){
          //tester mode
          console.log("changmenet Lifelist")
          lifelist = Life.list.map((y,ynum)=>y.map( 
              (w,wnum)=> {if (wnum>=LifeBoard.cercledeb[1] && 
                              wnum<=LifeBoard.cerclefin[1] &&
                              ynum>=LifeBoard.cercledeb[0] && 
                              ynum<=LifeBoard.cerclefin[0]){
                                return Life.pactive
                              }
                              else{return w}
                          } ))
        }
        return m("content",{style:"width:800; margin:auto",onmouseup:()=>{LifeBoard.updateLife()}},
          m("#identity",{style:"text-align:center;font-weight:bold"},Life.id),
          m("#qrcode",{style:"text-align:center;font-weight:bold"}),
          m("#legende",
            m("ul#listeLegende",Life.legende.map((desc,i)=>m("li#p"+i+".periode",{style:i==Life.pactive?"border:solid 1px black":""},
              m('label',{for:"eve"+i,onclick:()=>{Life.pactive=i}},desc[0]),
              m("input",{type:"color",name:"eve"+i,value:desc[1],onchange:(e)=>Life.legende[i][1]=e.target.value}),
              )),
              m('li#newev', m("span#pres"
                , {style:"border:solid 1px black",onclick:function(){Life.legende.push([LifeBoard.nevName,LifeBoard.nevColor])}}
                , "➕"),
              m('input',{type:"text",for:"neve",style:"padding:1px 0",value:LifeBoard.nevName,onchange:(e)=>LifeBoard.nevName=e.target.value}),
              m("input",{type:"color",name:"neve",value:LifeBoard.nevColor,onchange:(e)=>LifeBoard.nevColor=e.target.value})
              )
              ),
            ),
          m("#board",
            m("svg[viewBox='0 0 800 1080'][xmlns='http://www.w3.org/2000/svg'][xmlns:xlink='http://www.w3.org/1999/xlink']", 
            
            lifelist.map(
              (y,ynum)=>y.map( 
              (w,wnum)=>m(
                "circle",
                {
                  cx:wnum*12+5,
                  cy:ynum*12+5,r:5,
                  stroke:"black",
                  fill:w>-1?Life.legende[w][1]:"white",
                  onmouseover:()=>{LifeBoard.cerclefin=[ynum,wnum]},
                  onmousedown:()=>{LifeBoard.cercledeb=[ynum,wnum]},
                  
                },
                m("title",wnum+","+ynum))))
          
              )
          )
        )
    }
}

m.mount(document.body, LifeBoard);

