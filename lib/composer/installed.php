<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '40ad48d3fb11f8ef643fa0a6adb57b167a3b00de',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '40ad48d3fb11f8ef643fa0a6adb57b167a3b00de',
            'dev_requirement' => false,
        ),
        'bcosca/fatfree-core' => array(
            'pretty_version' => '3.8.1',
            'version' => '3.8.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../bcosca/fatfree-core',
            'aliases' => array(),
            'reference' => 'f6ea45472f55a1ecabd4ced74cb56f0efd6af34e',
            'dev_requirement' => false,
        ),
    ),
);
